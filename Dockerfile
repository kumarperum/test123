# Build image with:  docker build -t krizsan/ubuntu1504java7:v1 .
FROM ubuntu:15.04
MAINTAINER kumar
ADD target/*.jar App.jar
RUN useradd -ms /bin/bash cicduser
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/App.jar"]	
#RUN echo "Starting Install" && apt update && \
#    docker --version && \
#    apt install openjdk-11-jdk -y && \
#    docker run hello-worlds && \
#    echo "java installation copleted." && \
#    java -version
