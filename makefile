GIT_REPO := https://github.com/DuoDuoMelb/java-maven-junit-helloworld.git
DIR := "dev"
OS := $(shell uname)

build:  checkargbuild 
	#./scripts/my-important-task.sh my-parameter
	@echo "argument build."
	docker build -f Dockerfile -t  ${ARGS3}  .
	docker tag ${ARGS3} ${ARGS2}
	docker push  ${ARGS1}
	@echo "Build & Taging Completed.."
	
provision:  checkargprovision
	@echo    "provision starts."
	docker pull ${ARGS1}
	@echo    "provision Completed."
	
test: checkrun
	@echo    "Test starts."
	docker run -p ${PORT1}:${PORT2} ${ARGS2}
	##curl -i http://localhost:8085/rest/docker/hello
	@echo    "Test Ends."
	
clean:
	@echo    "Clean starts."
	mvn  clean
	
checkargbuild: ## Checks Variables to have values before doing main operations
	@test -n "${ARGS}"   || (printf "${RED}Error, Argument(ARGS) is not passed\n" && exit 1)
	@printf "ARGS: ${ARGS}\n"
	@test -n "${ARGS1}"   || (printf "${RED}Error, Argument(ARGS1) is not passed\n" && exit 1)
	@printf "ARGS1: ${ARGS1}\n"
	@test -n "${ARGS2}"   || (printf "${RED}Error, Argument(ARGS2) is not passed\n" && exit 1)
	@printf "ARGS2: ${ARGS2}\n"	
	@test -n "${ARGS3}"   || (printf "${RED}Error, Argument(ARGS3) is not passed\n" && exit 1)
	@printf "ARGS3: ${ARGS3}\n"	

checkargprovision:  ## Checks Variables to have values before doing main operations
	@test -n "${ARGS}"   || (printf "${RED}Error, Argument(ARGS) is not passed\n" && exit 1)
	@printf "ARGS: ${ARGS}\n"
	@test -n "${ARGS1}"   || (printf "${RED}Error, Argument(ARGS1) is not passed\n" && exit 1)
	@printf "ARGS1: ${ARGS1}\n"
	@test -n "${ARGS2}"   || (printf "${RED}Error, Argument(ARGS2) is not passed\n" && exit 1)
	@printf "ARGS2: ${ARGS2}\n"	
	@test -n "${ARGS3}"   || (printf "${RED}Error, Argument(ARGS3) is not passed\n" && exit 1)
	@printf "ARGS3: ${ARGS3}\n"	
	
checkrun:  ## Checks Variables to have values before doing main operations
	@test -n "${ARGS}"   || (printf "${RED}Error, Argument(ARGS) is not passed\n" && exit 1)
	@printf "ARGS: ${ARGS}\n"
	@test -n "${ARGS1}"   || (printf "${RED}Error, Argument(ARGS1) is not passed\n" && exit 1)
	@printf "ARGS1: ${ARGS1}\n"
	@test -n "${ARGS2}"   || (printf "${RED}Error, Argument(ARGS2) is not passed\n" && exit 1)
	@printf "ARGS2: ${ARGS2}\n"	
	@test -n "${ARGS3}"   || (printf "${RED}Error, Argument(ARGS3) is not passed\n" && exit 1)
	@printf "ARGS3: ${ARGS3}\n"	
	@test -n "${PORT1}"   || (printf "${RED}Error, Argument(PORT1) is not passed\n" && exit 1)
	@printf "PORT1: ${PORT1}\n"	
	@test -n "${PORT2}"   || (printf "${RED}Error, Argument(PORT2) is not passed\n" && exit 1)
	@printf "PORT2: ${PORT2}\n"	
	
